package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

import java.util.ArrayList;

/**
 * Pawn class is used to represent the pawn piece in chess. It is a subclass of Piece (@link Piece) and inherits its variables and methods. The class defines what move is considered legal for the pawn piece as well as implements its toString method for how the piece will appear on the board.
 *
 * @see Piece
 */

public class Pawn extends Piece {
    public Pawn(Pawn pawn) {
        super(pawn);
    }

    public Pawn(Color color, String col) {
        String row = color == Color.WHITE ? "6" : "1";
        this.color = color;
        this.position = String.format("cell%s%s", row, col);
    }

    public boolean validateMove(String move) {
        String[] positions = move.split("-");
        String startPosition = positions[0].substring(4, 6);
        String endPosition = positions[1].substring(4, 6);
        if (startPosition.charAt(1) != endPosition.charAt(1)) {
            return false;
        }
        if (this.color == Color.WHITE) {
            char initRow = '6';
            int rowDiff = startPosition.charAt(0) - endPosition.charAt(0);
            return ((rowDiff == 1) || (rowDiff == 2 && startPosition.charAt(0) == initRow));

        } else {
            char initRow = '1';
            int rowDiff = endPosition.charAt(0) - startPosition.charAt(0);
            return ((rowDiff == 1) || (rowDiff == 2 && startPosition.charAt(0) == initRow));
        }
    }

    public String[] availableMoves() {
        int initRow = this.color == Color.WHITE ? 6 : 1;
        int currentRow = Character.getNumericValue(this.position.charAt(4));
        int lastRow = this.color == Color.WHITE ? 0 : 7;
        int direction = this.color == Color.WHITE ? -1 : 1;
        ArrayList<String> allMoves = new ArrayList<>();

        if (currentRow == initRow) {
            int newRow = currentRow + (direction * 2);
            allMoves.add(String.format("cell%s%s", newRow, this.position.charAt(5)));
        }
        if (currentRow != lastRow) {
            int newRow = currentRow + direction;
            allMoves.add(String.format("cell%s%s", newRow, this.position.charAt(5)));
        }
        return allMoves.toArray(new String[allMoves.size()]);
    }
}
