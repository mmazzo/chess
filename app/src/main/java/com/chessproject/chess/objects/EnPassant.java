package com.chessproject.chess.objects;

import com.chessproject.chess.objects.pieces.Pawn;

public class EnPassant {
    /**
     * String position of where on the board the EnPassant will live.
     */
    private String position;

    /**
     * The Pawn that is associated with the EnPassant.
     */
    private Pawn pawn;

    /**
     * A getter method for getting the position of the EnPassant
     *
     * @return the position of the EnPassant.
     */
    public String getPosition() {
        return position;
    }

    /**
     * A setter method for setting the position of the EnPassant.
     *
     * @param position value that the position of the EnPassant will be set to.
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * A getter method for getting the associated Pawn for the EnPassant.
     *
     * @return the Pawn associated to the EnPassant.
     */
    public Pawn getPawn() {
        return pawn;
    }

    /**
     * A setter method for setting the Pawn associated to the EnPassant.
     *
     * @param pawn Pawn that is associated to the EnPassant.
     */
    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
    }
}
