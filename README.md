# CHESS
##### The Chess program is an Android application that allows the user to play, save, and replay a game of Chess. On the home screen a user will have the option to start a new game or replay a previously saved game. During the game the user can move pieces on the board by selecting different tiles. There are buttons that allow the user to undo moves, suggest a draw, resign, or select a move at random. After completion of a game the player(s) will be directed to the end game activity where they will have the option to save the previously played game or just return to the home screen.
##### Author: Mike Mazzola
##### Version: 2.0

## MainActivity
On the initial screen the user can choose to begin a new game by clicking "Play New Game", or replay a previously played game from the list of saved games. Each saved game will have a unique name, the winner of the game (Black/White/Draw), and the total number of moves of the game. Clicking on a saved game will give the option to delete the game or begin the replay. When a selection is made the user will be redirected to the ChessGameActivity.

## ChessGameActivity
When this activity is initialized it will display a chessboard with all pieces on their starting tiles. There is a circle below the board that will change colors (black/white) to indicate whose turn it is throughout the game.
###  Buttons around the board allow the user to:
1. Undo the previous move
2. Suggest a draw - Both players need to suggest a draw consecutively for the game to end in draw. Suggesting a draw will complete the players turn.
3. Resign from the game - The game will be ended in its current state and the opposing player will be crowned as the winner.
4. Randomly select a move - A move will be randomly selected for the current players turn.

###  The ChessGameActivity can be accessed through one of two ways:
1. Playing a game
2. Replaying a game

#### Playing A Game
In this mode the user has access to all buttons available on the screen and the chess game can be played as expected. Pieces on the board are moved by clicking on tiles, and will only move if the suggested move is confirmed to be a valid chess move.
#####  The game can be ended in several ways:
1. A player is put into checkmate
3. The players have reached a stalemate
4. One player resigns from the game
5. Both players suggest a draw consecutively
#### Replaying A Game
In this mode the ability to move pieces by clicking tiles is disabled as well as the buttons surrounding the board. A button appears in the center of the board that allows the uservto traverse the game until completion.

## EndGameActivity
Once the game is complete the user will be redirected to the EndGameActivity.

When this activity is initialized it will display "Game Over" text as well as the winner of the previously played game (Black/White/Draw). The user will be able to give the previous game a name and save it into cache, unless the game was a replay, in which this will be disabled. The name must be valid in order for the game to save successfully.
#####  Valid names must:
1. Not contain the character: "$"
3. Not match an already existing saved game
4. Not be blank

If saving is attempted while this criteria isn't met the user will be prompted with an alert "Error Saving - Invalid Name" and the game will not be saved. After saving successfully, the game the user will be redirected to the MainActivity. There is also a button labeled "Return To Home Screen" which will redirect the user to MainActivity without saving.

![Home Screen](app/screenshots/HomeScreen.png)
![Start of Game](app/screenshots/StartGame.png)
![Middle of Game](app/screenshots/MidGame.png)
![End Game](app/screenshots/EndGame.png)
![Select Saved Game](app/screenshots/SelectSavedGame.png)
![Replay Game](app/screenshots/ReplayGame.png)