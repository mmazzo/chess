package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

/**
 * Rook class is used to represent the rook piece in chess. It is a subclass of Piece (@link Piece) and inherits its variables and methods. The class defines what move is considered legal for the rook piece as well as implements its toString method for how the piece will appear on the board.
 *
 * @see Piece
 */

public class Rook extends Piece {
    public Rook(Rook rook) {
        super(rook);
    }

    public Rook(String position) {
        this.position = position;
    }

    public Rook(Color color, String col) {
        String row = color == Color.WHITE ? "7" : "0";

        this.color = color;
        this.position = String.format("cell%s%s", row, col);
    }

    public boolean validateMove(String move) {
        String[] positions = move.split("-");
        String startPosition = positions[0].substring(4, 6);
        String endPosition = positions[1].substring(4, 6);
        return ((int) startPosition.charAt(0) - (int) endPosition.charAt(0) == 0) || ((int) startPosition.charAt(1) - (int) endPosition.charAt(1) == 0);
    }

    public String[] availableMoves() {
        String allMovesString = "";
        int startRow = Character.getNumericValue(this.position.charAt(4));
        int startCol = Character.getNumericValue(this.position.charAt(5));

        int moveRow = 0;
        while (moveRow <= 7) {
            if (startRow != moveRow) {
                allMovesString = allMovesString + String.format("cell%s%s ", moveRow, startCol);
            }
            moveRow++;
        }
        int moveCol = 0;
        while (moveCol <= 7) {
            if (startCol != moveCol) {
                allMovesString = allMovesString + String.format("cell%s%s ", startRow, moveCol);
            }
            moveCol++;
        }
        return allMovesString.split(" ");
    }
}