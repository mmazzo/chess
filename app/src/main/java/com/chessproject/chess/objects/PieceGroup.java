package com.chessproject.chess.objects;

import com.chessproject.chess.enums.Color;
import com.chessproject.chess.objects.pieces.Bishop;
import com.chessproject.chess.objects.pieces.King;
import com.chessproject.chess.objects.pieces.Knight;
import com.chessproject.chess.objects.pieces.Pawn;
import com.chessproject.chess.objects.pieces.Piece;
import com.chessproject.chess.objects.pieces.Queen;
import com.chessproject.chess.objects.pieces.Rook;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PieceGroup implements Serializable {
    /**
     * King Piece for PieceGroup.
     */
    King king;
    /**
     * Queen Piece for PieceGroup.
     */
    Queen queen;
    /**
     * Rook Pieces for PieceGroup.
     */
    Rook rook1;
    Rook rook2;
    /**
     * Knight Pieces for PieceGroup.
     */
    Knight knight1;
    Knight knight2;
    /**
     * Bishop Pieces for PieceGroup.
     */
    Bishop bishop1;
    Bishop bishop2;
    /**
     * Pawn List for PieceGroup. These are type Piece instead of Pawn to handle pawn promotion.
     */
    ArrayList<Piece> pawns;

    public PieceGroup(Color color) {
        this.king = new King(color);
        this.queen = new Queen(color);
        this.bishop1 = new Bishop(color, "2");
        this.bishop2 = new Bishop(color, "5");
        this.knight1 = new Knight(color, "1");
        this.knight2 = new Knight(color, "6");
        this.rook1 = new Rook(color, "0");
        this.rook2 = new Rook(color, "7");
        this.pawns = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            this.pawns.add(new Pawn(color, i + ""));
        }
    }

    public PieceGroup(PieceGroup copyPieces) {
        this.king = new King(copyPieces.getKing());
        this.queen = new Queen(copyPieces.getQueen());
        this.bishop1 = new Bishop(copyPieces.getBishop1());
        this.bishop2 = new Bishop(copyPieces.getBishop2());
        this.knight1 = new Knight(copyPieces.getKnight1());
        this.knight2 = new Knight(copyPieces.getKnight2());
        this.rook1 = new Rook(copyPieces.getRook1());
        this.rook2 = new Rook(copyPieces.getRook2());
        this.pawns = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            if (copyPieces.getPawns().get(i).getClass().getSimpleName().equals("Queen")) {
                this.pawns.add(new Queen((Queen) copyPieces.getPawns().get(i)));
            } else {
                this.pawns.add(new Pawn((Pawn) copyPieces.getPawns().get(i)));
            }
        }
    }

    /**
     * A method for collecting all Pieces in the PieceGroup whose position is not null.
     *
     * @return List of Pieces still on board.
     */
    public ArrayList<Piece> getAllAvailablePieces() {
        ArrayList<Piece> pieces = new ArrayList<>();
        for (Piece piece : getAllPieces()) {
            if (piece.getPosition() != null) {
                pieces.add(piece);
            }
        }
        return pieces;
    }

    /**
     * A method for collecting all Pieces in the PieceGroup regardless if their position is null.
     *
     * @return List of all Pieces in PieceGroup.
     */
    public ArrayList<Piece> getAllPieces() {
        ArrayList<Piece> pieces = new ArrayList<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value = null;
            try {
                value = field.get(this);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (value != null) {
                if (field.getName() != "pawns") {
                    Piece piece = ((Piece) value);
                    pieces.add(piece);
                } else {
                    for (Piece pawn : ((ArrayList<Piece>) value)) {
                        pieces.add(pawn);
                    }
                }
            }
        }
        return pieces;
    }

    /**
     * A method for getting a piece in the PieceGroup whose position matches the passed String.
     *
     * @param position of the Piece to be gotten.
     * @return the found Piece, null if no Piece is found.
     */
    public Piece getPiece(String position) {
        for (Piece piece : getAllAvailablePieces()) {
            if (piece.getPosition().equals(position)) {
                return piece;
            }
        }
        return null;
    }

    /**
     * A method for getting all the moves available for each piece in the PieceGroup.
     *
     * @return a Map of pieces paired with all their available moves.
     */
    public Map<Piece, String[]> allAvailableMoves() {
        Map<Piece, String[]> availableMoves = new HashMap<>();
        for (Piece piece : getAllAvailablePieces()) {
            availableMoves.put(piece, piece.availableMoves());
        }
        return availableMoves;
    }

    /**
     * Method for promoting a Pawn to a Queen.
     *
     * @param pawn  Pawn that will be promoted.
     * @param queen Queen that will replace the Pawn.
     */
    public void promotePawn(Pawn pawn, Queen queen) {
        pawns.set(pawns.indexOf(pawn), queen);
    }

    /**
     * A getter method for the King in the PieceGroup.
     *
     * @return the King Piece.
     */
    private King getKing() {
        return king;
    }

    /**
     * A getter method for the Queen in the PieceGroup.
     *
     * @return the Queen Piece.
     */
    private Queen getQueen() {
        return queen;
    }

    /**
     * A getter method for the Rook1 in the PieceGroup.
     *
     * @return the Rook Piece.
     */
    private Rook getRook1() {
        return rook1;
    }

    /**
     * A getter method for the Rook2 in the PieceGroup.
     *
     * @return the Rook Piece.
     */
    private Rook getRook2() {
        return rook2;
    }

    /**
     * A getter method for the Knight1 in the PieceGroup.
     *
     * @return the Knight Piece.
     */
    private Knight getKnight1() {
        return knight1;
    }

    /**
     * A getter method for the Knight2 in the PieceGroup.
     *
     * @return the Knight Piece.
     */
    private Knight getKnight2() {
        return knight2;
    }

    /**
     * A getter method for the Bishop1 in the PieceGroup.
     *
     * @return the Bishop Piece.
     */
    private Bishop getBishop1() {
        return bishop1;
    }

    /**
     * A getter method for the Bishop2 in the PieceGroup.
     *
     * @return the Bishop Piece.
     */
    private Bishop getBishop2() {
        return bishop2;
    }

    /**
     * A getter method for all Pawns in the PieceGroup.
     *
     * @return List of all Pawn Pieces.
     */
    private ArrayList<Piece> getPawns() {
        return pawns;
    }
}

