package com.chessproject.chess.enums;

/**
 * Enum of end game status indicating the winner of the game or if the game ended in a draw.
 */
public enum EndGame {
    BLACK_WINS("BLACK WINS"),
    WHITE_WINS("WHITE WINS"),
    DRAW("DRAW");

    private final String stringText;

    EndGame(String stringText) {
        this.stringText = stringText;
    }

    public static EndGame getColorWin(Color color) {
        return color == Color.WHITE ? WHITE_WINS : BLACK_WINS;
    }

    public String getStringText() {
        return stringText;
    }

    public String getWinnerShort() {
        String[] result = this.stringText.toLowerCase().split(" ");
        return result[0].substring(0, 1).toUpperCase() + result[0].substring(1);
    }
}
