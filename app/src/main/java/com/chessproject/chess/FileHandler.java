package com.chessproject.chess;

import com.chessproject.chess.objects.GameDetails;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class FileHandler {
    /**
     * Method for validating the game being saved meets all criteria to save a new game and saving
     * it to the cache file if it does.
     *
     * @param text     String value comprised of game information in the format: <NAME>$<WINNER>$<MOVES>.
     * @param cacheDir Cache directory for saved game information.
     * @return boolean indicating if the save was successful or not.
     */
    public static boolean attemptToSaveGame(String text, File cacheDir) {
        String savedGame = text.substring(0, text.indexOf('$'));
        if (savedGame.length() <= 1 || savedGame.contains("$")) {
            return false;
        }
        for (String gameLine : getAllGameLines(cacheDir)) {
            if (gameLine.contains(String.format("%s$", savedGame))) {
                return false;
            }
        }
        return saveToFile(text, cacheDir);
    }

    /**
     * Method for getting game moves for a specific saved game. The game is accessed by its
     * line number in the cache file.
     *
     * @param index    line number in cache file.
     * @param cacheDir Cache directory for saved game information.
     * @return List of moves for the specified game.
     */
    public static ArrayList<String> getGameMoves(int index, File cacheDir) {
        String gameLine = getAllGameLines(cacheDir).get(index);
        String[] gameLineArray = gameLine.split("\\$");
        String gameMoves = gameLineArray[2];
        return new ArrayList<>(Arrays.asList(gameMoves.substring(1, gameMoves.length() - 1).split(", ")));
    }

    /**
     * Method for collecting all game information from cache file and creating a List of
     * GameDetails.
     *
     * @param cacheDir Cache directory for saved game information.
     * @return GameDetails List containing all saved games.
     */
    public static ArrayList<GameDetails> getAllGameDetails(File cacheDir) {
        ArrayList<GameDetails> allGameDetails = new ArrayList<>();
        for (String game : getAllGameLines(cacheDir)) {
            allGameDetails.add(getGameDetails(game));
        }
        return allGameDetails;
    }

    /**
     * Method to delete a game by removing a specific line from the cache file.
     *
     * @param cacheDir   Cache directory for saved game information.
     * @param lineNumber number indicating the line to retrieve from the cache file.
     */
    public static void deleteGame(File cacheDir, int lineNumber) {
        ArrayList<String> fileLines = getAllGameLines(cacheDir);
        try {
            final File file = new File(cacheDir, File.separator + "SavedGames.txt");
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
            BufferedWriter bw;
            file.createNewFile();
            bw = new BufferedWriter(fw);
            for (int i = 0; i < fileLines.size(); i++) {
                if (i != lineNumber) {
                    bw.write(fileLines.get(i));
                    bw.newLine();
                }
            }
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method for getting GameDetails from a game line found in the cache.
     *
     * @param gameLine saved game information found in the cache file.
     * @return GameDetails object populated with data from the passed gameLine.
     */
    private static GameDetails getGameDetails(String gameLine) {
        String[] gameArray = gameLine.split("\\$");
        return new GameDetails(gameArray[0], gameArray[1], gameArray[2].split(", ").length);
    }

    /**
     * Method for reading the save file and creating a List of all the saved games.
     *
     * @param cacheDir Cache directory for saved game information.
     * @return List of all lines in file of saved games
     */
    private static ArrayList<String> getAllGameLines(File cacheDir) {
        ArrayList<String> fileLines = new ArrayList<>();
        BufferedReader br;
        FileReader fr;
        try {
            fr = new FileReader(
                    cacheDir + File.separator + "SavedGames.txt");
            br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                fileLines.add(line);
                line = br.readLine();
            }
            br.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileLines;
    }

    /**
     * Save the game into a cache file.
     *
     * @param text     String value comprised of game information in the format: <NAME>$<WINNER>$<MOVES>.
     * @param cacheDir Cache directory for saved game information.
     * @return boolean value indicating if the save was successful.
     */
    private static boolean saveToFile(String text, File cacheDir) {
        final File file = new File(cacheDir, File.separator + "SavedGames.txt");
        try {
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw;
            if (!file.exists()) {
                file.createNewFile();
            }
            bw = new BufferedWriter(fw);
            bw.write(text);
            bw.newLine();
            bw.close();
            fw.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
