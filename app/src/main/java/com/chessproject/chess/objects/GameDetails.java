package com.chessproject.chess.objects;

public class GameDetails {
    /**
     * The name of game that is being saved.
     */
    private String gameName;
    /**
     * The winner of the game.
     */
    private String winner;
    /**
     * The number of moves executed throughout the game.
     */
    private int numberOfMoves;

    public GameDetails(String gameName, String winner, int numberOfMoves) {
        this.gameName = gameName;
        this.winner = winner;
        this.numberOfMoves = numberOfMoves;
    }

    /**
     * A getter method for getting the name of the game.
     *
     * @return winner of the game.
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * A getter method for getting the winner of the game.
     *
     * @return the winner of the game.
     */
    public String getWinner() {
        return winner;
    }

    /**
     * A getter method for getting the number of moves executed during the game.
     *
     * @return number of moves.
     */
    public int getNumberOfMoves() {
        return numberOfMoves;
    }
}
