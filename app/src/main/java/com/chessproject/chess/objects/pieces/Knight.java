package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Knight class is used to represent the knight piece in chess. It is a subclass of Piece (@link Piece) and inherits its variables and methods. The class defines what move is considered legal for the knight piece as well as implements its toString method for how the piece will appear on the board.
 *
 * @see Piece
 */

public class Knight extends Piece {
    public Knight(Knight knight) {
        super(knight);
    }

    public Knight(Color color, String col) {
        String row = color == Color.WHITE ? "7" : "0";
        this.color = color;
        this.position = String.format("cell%s%s", row, col);
    }

    public boolean validateMove(String move) {
        String[] positions = move.split("-");
        String startPosition = positions[0].substring(4, 6);
        String endPosition = positions[1].substring(4, 6);
        int rowDiff = Math.abs((int) startPosition.charAt(0) - (int) endPosition.charAt(0));
        int colDiff = Math.abs((int) startPosition.charAt(1) - (int) endPosition.charAt(1));
        return (rowDiff == 2 && colDiff == 1) || (rowDiff == 1 && colDiff == 2);
    }

    public String[] availableMoves() {
        String allMovesString = "";
        ArrayList<Integer> moveArray = new ArrayList<Integer>(Arrays.asList(1, -1, 2, -2));
        ArrayList<Integer> subArray = new ArrayList<>();
        for (int i = 0; i < moveArray.size(); i++) {
            subArray.clear();
            subArray.addAll(moveArray);
            subArray.remove(moveArray.get(i));
            for (int j = 0; j < subArray.size(); j++) {
                if (!(Math.abs(moveArray.get(i)) == Math.abs(subArray.get(j)))) {
                    int moveRow = Character.getNumericValue(this.position.charAt(4)) + moveArray.get(i);
                    int moveCol = Character.getNumericValue(this.position.charAt(5)) + subArray.get(j);
                    if (0 <= moveRow && moveRow <= 7 && 0 <= moveCol && moveCol <= 7) {
                        allMovesString = allMovesString + String.format("cell%s%s ", moveRow, moveCol);
                    }
                }
            }
        }
        return allMovesString.split(" ");
    }
}
