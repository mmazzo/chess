package com.chessproject.chess.objects;

import com.chessproject.chess.enums.Color;

import java.io.Serializable;

public class PositionInstance implements Serializable {
    /**
     * PieceGroup for White Pieces in the PositionInstance.
     */
    final private PieceGroup whitePieces;
    /**
     * PieceGroup for Black Pieces in the PositionInstance.
     */
    final private PieceGroup blackPieces;
    /**
     * String of previous move that transitioned to this PositionInstance.
     */
    private String previousMove;
    /**
     * Color of the PieceGroup suggesting a draw.
     */
    private Color drawMove = null;

    public PositionInstance(PieceGroup whitePieces, PieceGroup blackPieces, Color color) {
        this.whitePieces = new PieceGroup(whitePieces);
        this.blackPieces = new PieceGroup(blackPieces);
        this.drawMove = color;
        this.previousMove = String.format("%s suggest", color.getStringText());
    }

    public PositionInstance(PieceGroup whitePieces, PieceGroup blackPieces, String previousMove) {
        this.whitePieces = new PieceGroup(whitePieces);
        this.blackPieces = new PieceGroup(blackPieces);
        this.previousMove = previousMove;
    }

    /**
     * A getter method for getting the White PieceGroup in the PositionInstance.
     *
     * @return White PieceGroup
     */
    public PieceGroup getWhitePieces() {
        return whitePieces;
    }

    /**
     * A getter method for getting the Black PieceGroup in the PositionInstance.
     *
     * @return Black Pieces
     */
    public PieceGroup getBlackPieces() {
        return blackPieces;
    }

    /**
     * A getter method for getting the move associated with the PositionInstance.
     *
     * @return the move that transitioned to this PositionInstance.
     */
    public String getPreviousMove() {
        return previousMove;
    }

    /**
     * A getter method for getting the color of the associated draw in the PositionInstance.
     *
     * @return the Color associated to the suggested draw.
     */
    public Color getDrawMove() {
        return drawMove;
    }
}
