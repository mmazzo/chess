package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * King class is used to represent the king piece in chess. It is a subclass of Piece (@link Piece) and inherits its variables and methods. The class defines what move is considered legal for the king piece as well as implements its toString method for how the piece will appear on the board.
 *
 * @see Piece
 */

public class King extends Piece {
    public King(King king) {
        super(king);
    }

    public King(Color color) {
        String kingStartCol = "4";
        String row = color == Color.WHITE ? "7" : "0";

        this.color = color;
        this.position = String.format("cell%s%s", row, kingStartCol);
    }

    public boolean validateMove(String move) {
        String[] positions = move.split("-");
        String startPosition = positions[0].substring(4, 6);
        String endPosition = positions[1].substring(4, 6);
        return (Math.abs((int) startPosition.charAt(0) - (int) endPosition.charAt(0)) <= 1) && (Math.abs((int) startPosition.charAt(1) - (int) endPosition.charAt(1)) <= 1);
    }

    public String[] availableMoves() {
        String allMovesString = "";
        ArrayList<Integer> moveArray = new ArrayList<Integer>(Arrays.asList(-1, 0, 1));
        ArrayList<Integer> subArray = new ArrayList<>();
        for (int i = 0; i < moveArray.size(); i++) {
            for (int j = 0; j < moveArray.size(); j++) {
                int moveRow = Character.getNumericValue(this.position.charAt(4)) + moveArray.get(i);
                int moveCol = Character.getNumericValue(this.position.charAt(5)) + moveArray.get(j);
                if ((0 <= moveRow && moveRow <= 7 && 0 <= moveCol && moveCol <= 7) && !(moveArray.get(i) == 0 && moveArray.get(j) == 0)) {
                    allMovesString = allMovesString + String.format("cell%s%s ", moveRow, moveCol);
                }
            }
        }
        return allMovesString.split(" ");
    }
}
