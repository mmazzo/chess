package com.chessproject.chess;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.chessproject.chess.enums.EndGame;
import com.chessproject.chess.objects.PositionInstance;

import java.io.File;
import java.util.ArrayList;

/**
 * <h2>EndGameActivity</h2>
 * When this activity is initialized it will display "Game Over" text as well as the winner of the
 * previously played game (Black/White/Draw). The user will be able to give the previous game a
 * name and save it into cache, unless the game was a replay, in which this will be disabled.
 * The name must be valid in order for the game to save successfully. Valid names must:
 * 1. Not contain the character: "$"
 * 2. Not match an already existing saved game
 * 3. Not be blank
 * If saving is attempted while this criteria isn't met the user will be prompted with an alert
 * "Error Saving - Invalid Name" and the game will not be saved. After saving successfully,
 * the game the user will be redirected to the MainActivity. There is also a button labeled
 * "Return To Home Screen" which will redirect the user to MainActivity without saving.
 *
 * @author Mike Mazzola
 * @version 2.0
 * @since 2020-05-01
 */

public class EndGameActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);
        TextView tvWinner = findViewById(R.id.tvWinner);
        Button saveButton = findViewById(R.id.btSave);
        final EditText gameName = findViewById(R.id.ptGameName);
        Button okButton = findViewById(R.id.btOK);
        final File cacheDir = this.getCacheDir();
        final EndGame endGameValue = (EndGame) getIntent().getSerializableExtra("Winner");
        final ArrayList positionList = (ArrayList) getIntent().getSerializableExtra("PositionList");
        final boolean saveEnabled = (boolean) getIntent().getSerializableExtra("SaveEnabled");
        tvWinner.setText(endGameValue.getStringText());
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameOfSavedGame = gameName.getText().toString();
                ArrayList<String> moves = new ArrayList<>();
                String winnerStr = endGameValue.getStringText();
                for (Object instance : positionList) {
                    String move = ((PositionInstance) instance).getPreviousMove();
                    if (move.length() == 13) {
                        moves.add(move);
                    }
                }
                String saveText = String.format("%s$%s$%s", nameOfSavedGame, endGameValue.getWinnerShort(), moves.toString());
                if (!FileHandler.attemptToSaveGame(saveText, cacheDir)) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EndGameActivity.this)
                            .setTitle("Error Saving - Invalid Name")
                            .setCancelable(true);
                    AlertDialog alertDialog = alertDialogBuilder.show();
                    alertDialog.getWindow().setLayout(900, 300);
                } else {
                    returnToHomeScreen();
                }
            }
        });
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToHomeScreen();
            }
        });
        saveButton.setEnabled(saveEnabled);
        gameName.setEnabled(saveEnabled);
    }

    @Override
    public void onBackPressed() {
        returnToHomeScreen();
    }

    /**
     * Change to the MainActivity.
     */
    private void returnToHomeScreen() {
        Intent intent = new Intent(EndGameActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
