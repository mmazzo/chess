package com.chessproject.chess;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.chessproject.chess.enums.Color;
import com.chessproject.chess.enums.EndGame;
import com.chessproject.chess.objects.EnPassant;
import com.chessproject.chess.objects.PieceGroup;
import com.chessproject.chess.objects.PositionInstance;
import com.chessproject.chess.objects.pieces.King;
import com.chessproject.chess.objects.pieces.Pawn;
import com.chessproject.chess.objects.pieces.Piece;
import com.chessproject.chess.objects.pieces.Queen;
import com.chessproject.chess.objects.pieces.Rook;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * <h2>ChessGameActivity</h2>
 * When this activity is initialized it will display a chessboard with all pieces on their starting
 * tiles. There is a circle below the board that will change colors (black/white) to indicate whose
 * turn it is throughout the game. There are buttons surrounding the board allowing the user to:
 * 1. Undo the previous move
 * 2. Suggest a draw - Both players need to suggest a draw consecutively for the game to
 * end in draw. Suggesting a draw will complete the players turn.
 * 3. Resign from the game - The game will be ended in its current state and the opposing player
 * will be crowned as the winner.
 * 4. Randomly select a move - A move will be randomly selected for the current players turn.
 * <p>
 * Once the game is complete the user will be redirected to the EndGameActivity.
 * <p>
 * The ChessGameActivity can be accessed through one of two ways:
 * 1. Playing a game
 * 2. Replaying a game
 *
 * <h3>Playing A Game</h3>
 * In this mode the user has access to all buttons available on the screen and the chess game can
 * be played as expected. Pieces on the board are moved by clicking on tiles, and will only move if
 * the suggested move is confirmed to be a valid chess move. The game can be ended in several ways:
 * 1. A player is put into checkmate
 * 2. The players have reached a stalemate
 * 3. One player resigns from the game
 * 4. Both players suggest a draw consecutively
 *
 * <h3>Replaying A Game</h3>
 * In this mode the ability to move pieces by clicking tiles is disabled as well as the buttons
 * surrounding the board. A button appears in the center of the board that allows the user
 * to traverse the game until completion.
 *
 * @author Mike Mazzola
 * @version 2.0
 * @since 2020-05-01
 */

public class ChessGameActivity extends AppCompatActivity {
    private PieceGroup whitePieces = new PieceGroup(Color.WHITE);
    private PieceGroup blackPieces = new PieceGroup(Color.BLACK);
    private Map<String, ImageView> imageMap = new HashMap<>();
    private EnPassant enPassant = new EnPassant();
    private ArrayList<PositionInstance> positionList = new ArrayList<>();
    private String currentMove = "";
    private Color colorTurn = Color.WHITE;
    private boolean whiteSuggestsDraw = false;
    private boolean blackSuggestsDraw = false;
    private boolean saveEnabled = true;
    private GradientDrawable colorTurnIndicatorBackground;

    /**
     * Assign roles to all buttons, create all black and white pieces, and place images on the
     * applicable positions.
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess_game);
        colorTurnIndicatorBackground = (GradientDrawable) findViewById(R.id.colorTurnIndicator).getBackground();
        colorTurnIndicatorBackground.setColor(colorTurn.getAndroidColor());

        Button undoBt = findViewById(R.id.btUndo);
        undoBt.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (positionList.size() > 1) {
                    PositionInstance previousInstance = positionList.get(positionList.size() - 2);
                    revertMove(previousInstance);
                }
            }
        });
        Button drawBt = findViewById(R.id.btDraw);
        drawBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (colorTurn == Color.WHITE) {
                    whiteSuggestsDraw = true;
                } else {
                    blackSuggestsDraw = true;
                }
                positionList.add(new PositionInstance(whitePieces, blackPieces, colorTurn));
                checkEndGameDraw();
                colorTurn = colorTurn.getOppositeColor();
                colorTurnIndicatorBackground.setColor(colorTurn.getAndroidColor());
            }
        });
        Button resignBt = findViewById(R.id.btResign);
        resignBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionList.add(new PositionInstance(whitePieces, blackPieces, String.format("%s resigns", colorTurn.getStringText())));
                Color winningColor = colorTurn.getOppositeColor();
                endGame(EndGame.getColorWin(winningColor));
            }
        });
        Button randomMoveBt = findViewById(R.id.btRandom);
        randomMoveBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PieceGroup pieces = getColorPieces(colorTurn);
                ArrayList<String> allMoves = new ArrayList<>();
                for (Map.Entry<Piece, String[]> entry : getAllPossibleMoves(pieces).entrySet()) {
                    for (String endPosition : entry.getValue()) {
                        allMoves.add(String.format("%s-%s", entry.getKey().getPosition(), endPosition));
                    }
                }
                Color oppositeColor = colorTurn.getOppositeColor();
                for (int i = 0; i < allMoves.size(); i++) {
                    int random = (int) (Math.random() * allMoves.size());
                    attemptMove(allMoves.get(random));
                    if (colorTurn == oppositeColor) {
                        break;
                    }
                    allMoves.remove(random);
                }
            }
        });

        final Optional<ArrayList> replayMoves = Optional.ofNullable((ArrayList) getIntent().getSerializableExtra("ReplayMoves"));

        final Button btGameReplay = findViewById(R.id.btGameReplay);
        btGameReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!replayMoves.isPresent()) {
                    endGame(EndGame.DRAW);
                } else {
                    String nextMove = (String) replayMoves.get().get(0);
                    if (nextMove.contains("suggest")) {
                        String drawColor = nextMove.split(" ")[0];
                        if (drawColor.equals(Color.WHITE.getStringText())) {
                            whiteSuggestsDraw = true;
                        } else {
                            blackSuggestsDraw = true;
                        }
                        colorTurn = colorTurn.getOppositeColor();
                        colorTurnIndicatorBackground.setColor(colorTurn.getAndroidColor());

                    } else if (nextMove.contains("resigns")) {
                        Color winningColor = colorTurn.getOppositeColor();
                        endGame(EndGame.getColorWin(winningColor));
                    } else {
                        attemptMove(nextMove);
                    }

                    if (isColorInMate(colorTurn) && !isColorInCheck(colorTurn)) {
                        endGame(EndGame.DRAW);
                    }
                    checkEndGameDraw();
                    btGameReplay.setText(String.format("%s Move", colorTurn.getStringText()));
                    replayMoves.get().remove(0);
                }
            }
        });
        if (replayMoves.isPresent()) {
            randomMoveBt.setEnabled(false);
            undoBt.setEnabled(false);
            drawBt.setEnabled(false);
            resignBt.setEnabled(false);
            btGameReplay.setVisibility(View.VISIBLE);
            saveEnabled = false;
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                final String cell = String.format("cell%s%s", i, j);
                int id = getResources().getIdentifier(cell, "id", getPackageName());
                imageMap.put(cell, (ImageView) findViewById(id));
                imageMap.get(cell).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent me) {
                        currentMove = currentMove.equals("") ? cell : currentMove + String.format("-%s", cell);
                        if (currentMove.contains("-")) {
                            attemptMove(currentMove);
                            currentMove = "";
                            if (isColorInMate(colorTurn) && !isColorInCheck(colorTurn)) {
                                endGame(EndGame.DRAW);
                            }
                        }
                        return false;
                    }
                });
                imageMap.get(cell).setEnabled(saveEnabled);
            }
        }
        setImages(whitePieces);
        setImages(blackPieces);
        positionList.add(new PositionInstance(whitePieces, blackPieces, ""));
    }

    /**
     * Add all images to applicable Imageviews so they appear on the board in their expected
     * position.
     *
     * @param pieces The group of pieces that contain the position of the images that need to be
     *               added to the board.
     */
    private void setImages(PieceGroup pieces) {
        for (Piece piece : pieces.getAllAvailablePieces()) {
            String image = (piece.getColor().toString() + "_" + piece.getClass().getSimpleName()).toLowerCase();
            imageMap.get(piece.getPosition()).setImageDrawable(getResources().getDrawable(getResources().getIdentifier(image, "drawable", getPackageName())));
        }
    }

    /**
     * Check if both white and black are have suggested a draw. If both have end the game in a draw.
     */
    private void checkEndGameDraw() {
        if (whiteSuggestsDraw && blackSuggestsDraw) {
            endGame(EndGame.DRAW);
        }
    }

    /**
     * Revert the pieces and images to the previous instance and remove the instance from the list.
     *
     * @param previousInstance the instance that the board pieces need to be reverted to.
     */
    private void revertMove(PositionInstance previousInstance) {
        whitePieces = new PieceGroup(previousInstance.getWhitePieces());
        blackPieces = new PieceGroup(previousInstance.getBlackPieces());
        Optional<Color> drawMove = Optional.ofNullable(previousInstance.getDrawMove());
        for (Map.Entry<String, ImageView> entry : imageMap.entrySet()) {
            Optional<Piece> whitePiece = Optional.ofNullable(whitePieces.getPiece(entry.getKey()));
            Optional<Piece> blackPiece = Optional.ofNullable(blackPieces.getPiece(entry.getKey()));
            if (whitePiece.isPresent()) {
                String image = (whitePiece.get().getColor().toString() + "_" + whitePiece.get().getClass().getSimpleName()).toLowerCase();
                entry.getValue().setImageDrawable(getResources().getDrawable(getResources().getIdentifier(image, "drawable", getPackageName())));
            } else if (blackPiece.isPresent()) {
                String image = (blackPiece.get().getColor().toString() + "_" + blackPiece.get().getClass().getSimpleName()).toLowerCase();
                entry.getValue().setImageDrawable(getResources().getDrawable(getResources().getIdentifier(image, "drawable", getPackageName())));
            } else {
                entry.getValue().setImageDrawable(null);
            }
        }
        if (drawMove.isPresent()) {
            if (drawMove.get() == Color.WHITE) {
                whiteSuggestsDraw = true;
            } else {
                blackSuggestsDraw = true;
            }
        } else {
            whiteSuggestsDraw = false;
            blackSuggestsDraw = false;
        }
        positionList.remove(positionList.size() - 1);
        colorTurn = colorTurn.getOppositeColor();
        colorTurnIndicatorBackground.setColor(colorTurn.getAndroidColor());
    }

    /**
     * Attempt to move a Piece from one tile to another. Check the validity of the move assessing
     * all applicable positions to ensure that the move is legal and possible.
     * If all conditions are met execute the move.
     *
     * @param move String composed of an origin and destination position.
     */
    private void attemptMove(String move) {
        whiteSuggestsDraw = false;
        blackSuggestsDraw = false;
        String[] positions = move.split("-");
        Piece movingPiece = getPiece(positions[0]);
        Optional<Piece> piece = Optional.ofNullable(getPiece(positions[0]));
        if (piece.isPresent() && piece.get().getColor() == colorTurn) {
            if ((piece.get().validateMove(move) && isMovePossible(move)) || isPawnAbleToAttack(move)) {
                if (executeMoveIfNotPutInCheck(move)) {
                    adjustImages(move);
                    colorTurn = colorTurn.getOppositeColor();
                    if (isColorInMate(colorTurn)) {
                        endGame(EndGame.getColorWin(movingPiece.getColor()));
                    }
                    colorTurnIndicatorBackground.setColor(colorTurn.getAndroidColor());
                }
            } else if (isCastlingPossible(move)) {
                executeCastleMove(move);
                colorTurn = colorTurn.getOppositeColor();
                colorTurnIndicatorBackground.setColor(colorTurn.getAndroidColor());
            }
        }
    }

    /**
     * Perform the passed move. If the move results in the player being placed in check, revert
     * the move and return false.
     *
     * @param move String composed of an origin and destination position.
     * @return boolean value indicating if the move was executed successfully.
     */
    private boolean executeMoveIfNotPutInCheck(String move) {
        String[] positions = move.split("-");
        Piece movingPiece = getPiece(positions[0]);
        Optional<Piece> opposingPiece = Optional.ofNullable(getPiece(positions[1]));
        if (opposingPiece.isPresent()) {
            opposingPiece.get().setPosition(null);
        }
        movingPiece.setPosition(positions[1]);
        if (isColorInCheck(movingPiece.getColor())) {
            movingPiece.setPosition(positions[0]);
            if (opposingPiece.isPresent()) {
                opposingPiece.get().setPosition(positions[1]);
            }
            return false;
        } else {
            String pieceClass = movingPiece.getClass().getSimpleName();
            int startRow = Character.getNumericValue(positions[0].charAt(4));
            int endRow = Character.getNumericValue(positions[1].charAt(4));
            Optional<String> enPassantPosition = Optional.ofNullable(enPassant.getPosition());
            if (enPassantPosition.isPresent() && enPassantPosition.get().equals(positions[1])) {
                imageMap.get(enPassant.getPawn().getPosition()).setImageDrawable(null);
                enPassant.getPawn().setPosition(null);
            }
            enPassant.setPosition(null);
            enPassant.setPawn(null);
            if (pieceClass.equals("Pawn")) {
                if (Math.abs(startRow - endRow) == 2) {
                    int direction = movingPiece.getColor() == Color.WHITE ? -1 : 1;
                    String middlePosition = String.format("cell%s%s", startRow + direction, positions[0].charAt(5));
                    enPassant.setPosition(middlePosition);
                    enPassant.setPawn((Pawn) movingPiece);
                }
                int lastRow = movingPiece.getColor() == Color.WHITE ? 0 : 7;
                if (lastRow == endRow) {
                    promotePawn((Pawn) movingPiece);
                }
            }
        }
        positionList.add(new PositionInstance(whitePieces, blackPieces, move));
        return true;
    }

    /**
     * Retrieve the Piece found at a specific cell.
     *
     * @param position cell number of a potential Piece.
     * @return the found Piece or null if no piece was found.
     */
    private Piece getPiece(String position) {
        return whitePieces.getPiece(position) != null ? whitePieces.getPiece(position) : blackPieces.getPiece(position);
    }

    /**
     * Get the PieceGroup matching the passed Color.
     *
     * @param color color of the PieceGroup.
     * @return the matching PieceGroup.
     */
    private PieceGroup getColorPieces(Color color) {
        return color == Color.WHITE ? whitePieces : blackPieces;
    }

    /**
     * Adjust the images to match passed move.
     *
     * @param move String composed of an origin and destination position.
     */
    private void adjustImages(String move) {
        String[] positions = move.split("-");
        Piece newPosition = getPiece(positions[1]);
        getPiece(positions[1]).setMoveCount(getPiece(positions[1]).getMoveCount() + 1);
        imageMap.get(positions[0]).setImageDrawable(null);
        String image = (newPosition.getColor().toString() + "_" + newPosition.getClass().getSimpleName()).toLowerCase();
        imageMap.get(positions[1]).setImageDrawable(getResources().getDrawable(getResources().getIdentifier(image, "drawable", getPackageName())));
    }

    /**
     * Validate a move to check if it is possible in terms of terms of the current board layout.
     *
     * @param move String composed of an origin and destination position.
     * @return boolean value indicating if the move is possible or not.
     */
    private boolean isMovePossible(String move) {
        String[] positions = move.split("-");
        Piece origin = getPiece(positions[0]);
        Piece destination = getPiece(positions[1]);
        if (destination != null && origin.getColor() == destination.getColor()) {
            return false;
        }
        switch (origin.getClass().getSimpleName()) {
            case "Queen":
            case "Bishop":
            case "Rook":
                return isSlideMovePossible(move);
            case "Pawn":
                return isPawnMovePossible(move);
            default:
                return true;
        }
    }

    /**
     * Determine if the passed Color is currently in check.
     *
     * @param color Color matching the PieceGroup being inspected.
     * @return boolean indicating whether or not the PieceGroup is in check
     */
    private Boolean isColorInCheck(Color color) {
        PieceGroup currentPieces = getColorPieces(color);
        PieceGroup opposingPieces = getColorPieces(color.getOppositeColor());
        Optional<King> currentKing;
        try {
            Field kingField = currentPieces.getClass().getDeclaredField("king");
            kingField.setAccessible(true);
            currentKing = Optional.ofNullable((King) kingField.get(currentPieces));

            for (Map.Entry<Piece, String[]> entry : getAllPossibleMoves(opposingPieces).entrySet()) {
                if (currentKing.isPresent() && Arrays.asList(entry.getValue()).contains(currentKing.get().getPosition())) {
                    return true;
                }
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Determine if the PieceGroup matching the passed color is able to make any moves without
     * placing themselves into check.
     *
     * @param color color of the PieceGroup being inspected.
     * @return boolean indicating whether or not the PieceGroup is in mate.
     */
    private Boolean isColorInMate(Color color) {
        PieceGroup pieces = getColorPieces(color);
        for (Map.Entry<Piece, String[]> entry : getAllPossibleMoves(pieces).entrySet()) {
            String startPosition = entry.getKey().getPosition();
            for (String movePosition : entry.getValue()) {
                Optional<Piece> pieceInPosition = Optional.ofNullable(getPiece(movePosition));
                if (pieceInPosition.isPresent()) {
                    pieceInPosition.get().setPosition(null);
                }
                entry.getKey().setPosition(movePosition);
                Boolean result = isColorInCheck(entry.getKey().getColor());
                entry.getKey().setPosition(startPosition);
                if (pieceInPosition.isPresent()) {
                    pieceInPosition.get().setPosition(movePosition);
                }
                if (!result) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Collect all possible moves for the passed PieceGroup.
     *
     * @param pieces PieceGroup used to gather all possible moves.
     * @return a Map of pieces and a String[] consisting of all available moves for the associated
     * piece.
     */
    private Map<Piece, String[]> getAllPossibleMoves(PieceGroup pieces) {
        Map<Piece, String[]> allPossibleMoves = new HashMap<>();
        for (Piece piece : pieces.allAvailableMoves().keySet()) {
            ArrayList<String> possibleMoves = new ArrayList<>();
            for (String cell : piece.availableMoves()) {
                String move = String.format("%s-%s", piece.getPosition(), cell);
                if (isMovePossible(move)) {
                    possibleMoves.add(cell);
                }
            }
            if (piece.getClass().getSimpleName().equals("Pawn")) {
                possibleMoves.addAll(getAllPossiblePawnAttacks((Pawn) piece));
            }
            int arraySize = possibleMoves.size();
            allPossibleMoves.put(piece, possibleMoves.toArray(new String[arraySize]));
        }
        return allPossibleMoves;
    }

    /**
     * Determine if a move made by Queens, Bishops, or Rooks will encounter another piece on the
     * path for the move. If a move exists on the path the move cannot be executed properly.
     *
     * @param move String composed of an origin and destination position.
     * @return boolean indicating if the move is possible.
     */
    private boolean isSlideMovePossible(String move) {
        String[] positions = move.split("-");
        int startRow = Character.getNumericValue(positions[0].charAt(4));
        int endRow = Character.getNumericValue(positions[1].charAt(4));
        int startCol = Character.getNumericValue(positions[0].charAt(5));
        int endCol = Character.getNumericValue(positions[1].charAt(5));
        int verticalDirection = Integer.compare(endRow, startRow);
        int horizontalDirection = Integer.compare(endCol, startCol);
        int iterationCount = Math.max(Math.abs(startRow - endRow), Math.abs(startCol - endCol));
        for (int i = 1; i < iterationCount; i++) {
            int currRow = startRow + i * verticalDirection;
            int currCol = startCol + i * horizontalDirection;
            Optional<Piece> pieceOnPath = Optional.ofNullable(getPiece(String.format("cell%s%s", currRow, currCol)));
            if (pieceOnPath.isPresent()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determine if a move for a Pawn is possible.
     *
     * @param move String composed of an origin and destination position.
     * @return boolean indicating if the move is possible.
     */
    public boolean isPawnMovePossible(String move) {
        String[] positions = move.split("-");
        Optional<Piece> destinationPositionPiece = Optional.ofNullable(getPiece(positions[1]));
        if ((positions[0].charAt(5) != positions[1].charAt(5)) || destinationPositionPiece.isPresent()) {
            return false;
        }
        int startRow = Character.getNumericValue(positions[0].charAt(4));
        int endRow = Character.getNumericValue(positions[1].charAt(4));
        if (Math.abs(startRow - endRow) == 2) {
            int midRow = Math.max(startRow, endRow) - 1;
            return getPiece(String.format("cell%s%s", midRow, positions[0].charAt(5))) == null;
        }
        return true;
    }

    /**
     * Determine if an attack for a Pawn is possible.
     *
     * @param move String composed of an origin and destination position.
     * @return boolean indicating if the attack is possible.
     */
    private boolean isPawnAbleToAttack(String move) {
        String[] positions = move.split("-");
        Piece piece = getPiece(positions[0]);
        if (!getPiece(positions[0]).getClass().getSimpleName().equals("Pawn")) {
            return false;
        }
        int startRow = Character.getNumericValue(positions[0].charAt(4));
        int endRow = Character.getNumericValue(positions[1].charAt(4));
        int startCol = Character.getNumericValue(positions[0].charAt(5));
        int endCol = Character.getNumericValue(positions[1].charAt(5));
        int direction = getPiece(positions[0]).getColor() == Color.WHITE ? 1 : -1;
        boolean movingForward = direction == startRow - endRow;
        boolean movingHorizontal = Math.abs(startCol - endCol) == 1;
        boolean attackingPiece = getPiece(positions[1]) != null && getPiece(positions[1]).getColor() != piece.getColor();
        boolean attackingEnPassant = positions[1].equals(enPassant.getPosition()) && enPassant.getPawn().getColor() != piece.getColor();
        return movingForward && movingHorizontal && (attackingPiece || attackingEnPassant);
    }

    /**
     * Collect all available attacks for the passed Pawn.
     *
     * @param pawn Pawn that is being checked for available attacks
     * @return List of all attacks possible for passed Pawn
     */
    private ArrayList<String> getAllPossiblePawnAttacks(Pawn pawn) {
        ArrayList<String> possibleAttacks = new ArrayList<>();
        int direction = pawn.getColor() == Color.WHITE ? -1 : 1;
        int startRow = Character.getNumericValue(pawn.getPosition().charAt(4));
        int startCol = Character.getNumericValue(pawn.getPosition().charAt(5));
        int endRow = startRow + direction;
        if (endRow <= 7 && endRow >= 0) {
            if (startCol < 7) {
                String attackPosition = String.format("cell%s%s", endRow, (startCol + 1));
                String move = String.format("%s-%s", pawn.getPosition(), attackPosition);
                if (isPawnAbleToAttack(move)) {
                    possibleAttacks.add(attackPosition);
                }
            }
            if (startCol > 0) {
                String attackPosition = String.format("cell%s%s", endRow, (startCol - 1));
                String move = String.format("%s-%s", pawn.getPosition(), attackPosition);
                if (isPawnAbleToAttack(move)) {
                    possibleAttacks.add(attackPosition);
                }
            }
        }
        return possibleAttacks;
    }

    /**
     * Promote the passed in Pawn to a Queen.
     *
     * @param pawn Pawn that needs to be promoted.
     */
    private void promotePawn(Pawn pawn) {
        PieceGroup pieces = getColorPieces(pawn.getColor());
        Queen queen = new Queen(pawn.getColor());
        queen.setPosition(pawn.getPosition());
        pawn.setPosition(null);
        pieces.promotePawn(pawn, queen);
    }

    /**
     * Determine if castling is possible for the given position.
     *
     * @param move String composed of an origin and destination position.
     * @return a boolean value representing if castling is possible.
     */
    private boolean isCastlingPossible(String move) {
        String[] positions = move.split("-");
        Optional<Piece> originPositionPiece = Optional.ofNullable(getPiece(positions[0]));
        Optional<Piece> destinationPositionPiece = Optional.ofNullable(getPiece(positions[1]));
        if (!originPositionPiece.isPresent() || !destinationPositionPiece.isPresent()) {
            return false;
        }
        String initKingPosition;
        King king;
        Rook rook;
        if (originPositionPiece.get().getClass().getSimpleName().equals("King") && destinationPositionPiece.get().getClass().getSimpleName().equals("Rook")) {
            king = (King) originPositionPiece.get();
            rook = (Rook) destinationPositionPiece.get();
            initKingPosition = positions[0];
        } else if (originPositionPiece.get().getClass().getSimpleName().equals("Rook") && destinationPositionPiece.get().getClass().getSimpleName().equals("King")) {
            king = (King) originPositionPiece.get();
            rook = (Rook) destinationPositionPiece.get();
            initKingPosition = positions[1];
        } else {
            return false;
        }
        int horizontalDirection = Character.getNumericValue(king.getPosition().charAt(5)) > Character.getNumericValue(rook.getPosition().charAt(5)) ? -1 : 1;
        if (king.getMoveCount() > 0 || rook.getMoveCount() > 0 || rook.getColor() != king.getColor()) {
            return false;
        }
        int currentCol = Character.getNumericValue(king.getPosition().charAt(5)) + horizontalDirection;
        while (currentCol != Character.getNumericValue(rook.getPosition().charAt(5))) {
            String position = String.format("cell%s%s", positions[0].charAt(4), currentCol);
            Optional<Piece> pieceOnPath = Optional.ofNullable(getPiece(position));
            if (pieceOnPath.isPresent()) {
                king.setPosition(initKingPosition);
                return false;
            }
            king.setPosition(position);
            if (isColorInCheck(king.getColor())) {
                king.setPosition(initKingPosition);
                return false;
            }
            currentCol = currentCol + horizontalDirection;
        }
        king.setPosition(initKingPosition);
        return true;
    }

    /**
     * Execute a castle move (called once castling is proven to be possible).
     *
     * @param move String indicating the position of the rook and king to be moved.
     */
    private void executeCastleMove(String move) {
        String[] positions = move.split("-");
        positionList.add(new PositionInstance(whitePieces, blackPieces, move));
        King king = null;
        Rook rook = null;
        if (getPiece(positions[0]).getClass().getSimpleName().equals("King") && getPiece(positions[1]).getClass().getSimpleName().equals("Rook")) {
            king = (King) getPiece(positions[0]);
            rook = (Rook) getPiece(positions[1]);
        } else if (getPiece(positions[0]).getClass().getSimpleName().equals("Rook") && getPiece(positions[1]).getClass().getSimpleName().equals("King")) {
            king = (King) getPiece(positions[1]);
            rook = (Rook) getPiece(positions[0]);
        }
        int horizontalDirection = Character.getNumericValue(king.getPosition().charAt(5)) > Character.getNumericValue(rook.getPosition().charAt(5)) ? -1 : 1;
        int initKingRow = Character.getNumericValue(king.getPosition().charAt(4));
        int initKingCol = Character.getNumericValue(king.getPosition().charAt(5));
        String newKingPosition = String.format("cell%s%s", initKingRow, (initKingCol + 2 * horizontalDirection));
        String newRookPosition = String.format("cell%s%s", initKingRow, (initKingCol + horizontalDirection));
        king.setPosition(newKingPosition);
        rook.setPosition(newRookPosition);
        king.setMoveCount(king.getMoveCount() + 1);
        rook.setMoveCount(rook.getMoveCount() + 1);
        imageMap.get(positions[0]).setImageDrawable(null);
        imageMap.get(positions[1]).setImageDrawable(null);
        String kingImage = (king.getColor().toString().toLowerCase() + "_king");
        imageMap.get(king.getPosition()).setImageDrawable(getResources().getDrawable(getResources().getIdentifier(kingImage, "drawable", getPackageName())));
        String rookImage = (rook.getColor().toString().toLowerCase() + "_rook");
        imageMap.get(rook.getPosition()).setImageDrawable(getResources().getDrawable(getResources().getIdentifier(rookImage, "drawable", getPackageName())));
    }

    /**
     * Once the game is completed change intent to EndGameActivity and pass it applicable
     * parameters.
     *
     * @param endGame is an EndGame object containing the end game state (winner/draw).
     */
    private void endGame(EndGame endGame) {
        Intent intent = new Intent(ChessGameActivity.this, EndGameActivity.class);
        intent.putExtra("Winner", endGame);
        intent.putExtra("PositionList", positionList);
        intent.putExtra("SaveEnabled", saveEnabled);
        startActivity(intent);
    }
}