package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

/**
 * Bishop class is used to represent the bishop piece in chess. It is a subclass of Piece (@link Piece) and inherits its variables and methods. The class defines what move is considered legal for the bishop piece as well as implements its toString method for how the piece will appear on the board.
 *
 * @see Piece
 */

public class Bishop extends Piece {
    public Bishop(Bishop bishop) {
        super(bishop);
    }

    public Bishop(String position) {
        this.position = position;
    }

    public Bishop(Color color, String col) {
        String row = color == Color.WHITE ? "7" : "0";
        this.color = color;
        this.position = String.format("cell%s%s", row, col);
    }

    public boolean validateMove(String move) {
        String[] positions = move.split("-");
        String startPosition = positions[0].substring(4, 6);
        String endPosition = positions[1].substring(4, 6);
        return Math.abs((int) startPosition.charAt(0) - (int) endPosition.charAt(0)) == Math.abs((int) startPosition.charAt(1) - (int) endPosition.charAt(1));
    }

    public String[] availableMoves() {
        String allMovesString = "";
        int startRow = Character.getNumericValue(this.position.charAt(4));
        int startCol = Character.getNumericValue(this.position.charAt(5));
        int dist = Math.min(startRow, (7 - startCol));
        int moveRow = startRow - dist;
        int moveCol = startCol + dist;
        while (moveRow <= 7 && moveCol >= 0) {
            if (!(startCol == moveCol && startRow == moveRow)) {
                allMovesString = allMovesString + String.format("cell%s%s ", moveRow, moveCol);
            }
            moveRow++;
            moveCol--;
        }
        dist = 7 - Math.max(startRow, startCol);
        moveRow = startRow + dist;
        moveCol = startCol + dist;
        while (moveRow >= 0 && moveCol >= 0) {
            if (!(startCol == moveCol && startRow == moveRow)) {
                allMovesString = allMovesString + String.format("cell%s%s ", moveRow, moveCol);
            }
            moveRow--;
            moveCol--;
        }
        return allMovesString.split(" ");
    }
}