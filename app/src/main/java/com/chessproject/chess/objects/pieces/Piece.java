package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

import java.io.Serializable;

public abstract class Piece implements Serializable {
    /**
     * Color value indicating the color of the Piece.
     */
    protected Color color;

    /**
     * The current position of the piece.
     */
    protected String position;

    /**
     * Integer value indicating the number of moves for the Piece.
     */
    protected int moveCount;

    protected Piece() {
        this.moveCount = 0;
    }

    protected Piece(Piece copyPiece) {
        this.color = copyPiece.getColor();
        this.position = copyPiece.getPosition();
        this.moveCount = copyPiece.getMoveCount();
    }

    /**
     * A getter method for getting the number of moves for the Piece.
     *
     * @return number of moves.
     */
    public int getMoveCount() {
        return moveCount;
    }

    /**
     * A setter method for setting the number of moves for the Piece.
     *
     * @param moveCount value the moveCount will be set to.
     */
    public void setMoveCount(int moveCount) {
        this.moveCount = moveCount;
    }

    /**
     * A getter method for getting the Color of the Piece.
     *
     * @return the Color of the Piece
     */
    public Color getColor() {
        return color;
    }

    /**
     * A setter method for setting the color of the piece.
     *
     * @param color value the Color will be set to.
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * A getter method for getting the position of the piece.
     *
     * @return the position of the Piece.
     */
    public String getPosition() {
        return position;
    }

    /**
     * A setter method for setting the position of the Piece.
     *
     * @param position value the position will be set to.
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * This method will check if a move is legal for the current Piece. Every piece has unique
     * moves and the pieces that inherit this class will implement their own format of legal moves.
     *
     * @return a boolean value that identifies if a move is legal.
     */
    public abstract boolean validateMove(String move);

    /**
     * This method will collect all available moves for the given piece (does not account
     * for other pieces on the board).
     *
     * @return a String array of all positions that a given piece can move to.
     */
    public abstract String[] availableMoves();
}

