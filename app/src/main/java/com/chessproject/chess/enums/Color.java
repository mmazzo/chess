package com.chessproject.chess.enums;

/**
 * Enum indicating the color of a specific Piece.
 */
public enum Color {
    BLACK("Black"), WHITE("White");

    private final String stringText;

    Color(String stringText) {
        this.stringText = stringText;
    }

    public String getStringText() {
        return stringText;
    }

    public Color getOppositeColor() {
        return this == Color.WHITE ? Color.BLACK : Color.WHITE;
    }

    public int getAndroidColor() {
        if (this.getStringText().equals("White")) {
            return android.graphics.Color.WHITE;
        } else {
            return android.graphics.Color.BLACK;
        }
    }
}
