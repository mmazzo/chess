package com.chessproject.chess.objects.pieces;

import com.chessproject.chess.enums.Color;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Queen class is used to represent the queen piece in chess. It is a subclass of Piece (@link Piece) and inherits its variables and methods. The class defines what move is considered legal for the queen piece as well as implements its toString method for how the piece will appear on the board.
 *
 * @see Piece
 */

public class Queen extends Piece {
    public Queen(Queen queen) {
        super(queen);
    }

    public Queen(Color color) {
        String queenStartCol = "3";
        String row = color == Color.WHITE ? "7" : "0";

        this.color = color;
        this.position = String.format("cell%s%s", row, queenStartCol);
    }

    public boolean validateMove(String move) {
        String[] positions = move.split("-");
        String startPosition = positions[0].substring(4, 6);
        String endPosition = positions[1].substring(4, 6);
        boolean diagonal = Math.abs((int) startPosition.charAt(0) - (int) endPosition.charAt(0)) == Math.abs((int) startPosition.charAt(1) - (int) endPosition.charAt(1));
        boolean straight = ((int) startPosition.charAt(0) - (int) endPosition.charAt(0) == 0) || ((int) startPosition.charAt(1) - (int) endPosition.charAt(1) == 0);
        return diagonal || straight;
    }

    public String[] availableMoves() {
        String[] straightMoves = new Rook(this.position).availableMoves();
        String[] diagonalMoves = new Bishop(this.position).availableMoves();
        ArrayList<String> queenMoves = new ArrayList<>();
        queenMoves.addAll(Arrays.asList(straightMoves));
        queenMoves.addAll(Arrays.asList(diagonalMoves));
        return Arrays.copyOf(queenMoves.toArray(), queenMoves.size(), String[].class);
    }

}