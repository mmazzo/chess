package com.chessproject.chess;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.chessproject.chess.objects.GameDetails;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Chess</h1>
 * The Chess program is an Android application that allows the user to play, save, and
 * replay a game of Chess. On the home screen a user will have the option to start a new
 * game or replay a previously saved game. When a game is initiated the program will switch
 * to the game activity. After completion of a game the player(s) will be directed to the
 * end game activity where they will have the option to save the previously played game
 * or just return to the home screen.
 *
 * <h2>MainActivity</h2>
 * On the initial screen the user can choose to begin a new game by clicking "Play New Game",
 * or replay a previously played game from the list of saved games. Each saved game will have
 * a unique name, the winner of the game (Black/White/Draw), and the total number of moves of the
 * game. Clicking on a saved game will give the option to delete the game or begin the replay. When
 * a selection is made the user will be redirected to the ChessGameActivity.
 *
 * @author Mike Mazzola
 * @version 2.0
 * @since 2020-05-01
 */

public class MainActivity extends AppCompatActivity {
    /**
     * Method for creating a SimpleAdapter to populate the ListView with all previous game details.
     *
     * @param cacheDir Cache directory for saved game information.
     * @param context  Context for the SimpleAdapter.
     * @return SimpleAdapter with populated data.
     */
    public static SimpleAdapter getSavedGameNames(File cacheDir, Context context) {
        final ArrayList<Map<String, String>> savedGamesList = new ArrayList<>();
        for (GameDetails game : FileHandler.getAllGameDetails(cacheDir)) {
            Map<String, String> gameMap = new HashMap<String, String>(2);
            gameMap.put("gameName", game.getGameName());
            String subDetail = String.format("Winner: %s | Number of moves: %s", game.getWinner(), game.getNumberOfMoves());
            gameMap.put("gameDetail", subDetail);
            savedGamesList.add(gameMap);
        }
        final SimpleAdapter adapter = new SimpleAdapter(context,
                savedGamesList, android.R.layout.simple_list_item_2,
                new String[]{"gameName", "gameDetail"},
                new int[]{android.R.id.text1, android.R.id.text2});

        return adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startButton = findViewById(R.id.btPlay);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ChessGameActivity.class);
                startActivity(intent);
            }
        });
        final ListView gameListView = findViewById(R.id.lvGames);
        gameListView.setAdapter(getSavedGameNames(getCacheDir(), this));
        gameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Select an option.")
                        .setCancelable(true)
                        .setPositiveButton("Replay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        initiateReplay(FileHandler.getGameMoves(i, getCacheDir()));
                                    }
                                })
                        .setNegativeButton("Delete",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        FileHandler.deleteGame(getCacheDir(), i);
                                        gameListView.setAdapter(getSavedGameNames(getCacheDir(), MainActivity.this));
                                    }
                                })
                        .setNeutralButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.show();
                alertDialog.getWindow().setLayout(900, 350);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    private void initiateReplay(ArrayList<String> moves) {
        Intent intent = new Intent(MainActivity.this, ChessGameActivity.class);
        intent.putStringArrayListExtra("ReplayMoves", moves);
        startActivity(intent);
    }
}